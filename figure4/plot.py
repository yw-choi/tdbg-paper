from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.path import Path
import matplotlib.cm as cm
import matplotlib as mpl

def main():
    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 6.0/2.54   # 10 cm to inch
    fig = plt.figure(figsize=(fw,fh))
    ax1 = fig.add_axes([0.13, 0.18, 0.67, 0.80])
    ax2 = fig.add_axes([0.82, 0.18, 0.05, 0.80])

    emax = 0.005
    efields = linspace(0.000, emax, 6)

    for ie, efield in enumerate(efields):

        data = loadtxt('./data/bw.%.3f_VPA.dat' % efield)

        c = cm.jet(efield/emax)

        # ax1.plot(data[:,0], data[:,1], '.-', c=c, label="%d $mV/\AA$" % (efield*1e3))
        ax1.plot(data[:,0], data[:,1], '.-', c=c)

    data = loadtxt('./data/TBG_bandwidths.dat')
    ax1.plot(data[:,0], data[:,1], '^-', c='C3')
    ax1.set_xlabel('$\\bf{\\theta}$ (deg.)', fontweight='bold')
    ax1.set_ylabel('Bandwidth (meV)', fontweight='bold')
    ax1.set_xlim(0.9, 1.7)
    ax1.set_ylim(0, 40)
    ax1.legend(frameon=False, loc='upper left')
    ax1.text(1.07, 30, 'TBG', fontsize=8, fontweight='bold', \
            va='center', ha='center', color='C3')
    ax1.text(1.47, 30, 'TDBG', fontsize=8, fontweight='bold', \
            va='center', ha='center', color=cm.jet(0))

    # ax2.set_axis_off()
    cmap = mpl.cm.jet
    norm = mpl.colors.Normalize(vmin=0.0, vmax=emax*1e3)
    cb1 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,
                                norm=norm,
                                orientation='vertical')
    cb1.set_label('Electric Field (mV/$\\bf{\AA}$)', fontweight='bold')

    plt.savefig('figure4.pdf')
    plt.savefig('figure4.eps')
    plt.show()

main()
