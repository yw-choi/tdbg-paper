from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches

def main():
    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 8.0/2.54   # 10 cm to inch
    fig = plt.figure(figsize=(fw,fh))

    gs = GridSpec(2,7)
    ax_a_band = plt.subplot(gs[0,:2])
    ax_a_dos  = plt.subplot(gs[0,2])
    ax_b_band = plt.subplot(gs[0,4:6])
    ax_b_dos  = plt.subplot(gs[0,6])
    ax_c = plt.subplot(gs[1,:3])
    ax_d = plt.subplot(gs[1,4:])

    left  = 0.18   # the left side of the subplots of the figure
    right = 0.96   # the right side of the subplots of the figure
    bottom = 0.15   # the bottom of the subplots of the figure
    top = 0.92     # the top of the subplots of the figure
    wspace = 0.0   # the amount of width reserved for blank space between subplots
    hspace = 0.3   # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left,
                        bottom=bottom,
                        right=right,
                        top=top,
                        wspace=wspace,
                        hspace=hspace)

    # subplot texts
    size=10
    x = -.55
    y = 1.16
    ax_a_band.text(x, y, '(a)', transform=ax_a_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.25
    ax_b_band.text(x, y, '(b)', transform=ax_b_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.35
    y = 1.08
    ax_c.text(x, y, '(c)', transform=ax_c.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.15
    y = 1.00
    ax_d.text(x, y, '(d)', transform=ax_d.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")

    Ry = 13.605693
    Ry2meV = Ry*1e3
    ymin = -40
    ymax =  40
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }

    enk = Ry*1e3*loadtxt('./data/M27N26.nonrlx.enk_band.dat')
    nk, nb = shape(enk)
    ib1 = 14
    ib2 = 17
    for ib in range(nb):
        if ib1<=ib and ib<=ib2:
            ax_a_band.plot(enk[:,ib], '-', c="C0", lw=.5)
        else:
            ax_a_band.plot(enk[:,ib], '-', c="C0", lw=.5)
    ax_a_band.grid(True, axis='x', **gridlinespec)
    ax_a_band.axhline(0,  **gridlinespec)
    ax_a_band.set_ylabel('Energy (meV)', ha='center', va='center')
    ax_a_band.yaxis.set_label_coords(-0.55, 0.5)
    ax_a_band.set_ylim(ymin, ymax)
    ax_a_band.set_xticklabels(["$\Gamma$", "$K$", "$M$", "$\Gamma$"])
    ax_a_band.set_xticks([0,30,60,90])
    ax_a_band.set_xlim(0,90)
    ax_a_band.set_title('Nonrelaxed', fontsize=8)
    ax_a_band.text(0.5, 0.65, '$\\theta=1.25^\circ$',
            transform=ax_a_band.transAxes, fontsize=7,
            fontweight='normal', va='center', ha='center',
            color="k")

    ax_a_dos.set_xticks([])
    ax_a_dos.set_yticks([])
    ax_a_dos.set_ylabel('')
    # ax_a_dos.set_xlabel('DOS', va='center', ha='center')
    # ax_a_dos.xaxis.set_label_coords(0.5, 1.08)
    ax_a_dos.set_title('DOS', fontsize=7)

    dos = loadtxt('./data/M27N26.nonrlx.edos.dat')
    ax_a_dos.plot(dos[:,1]/1e3, 1e3*dos[:,0], '-', c="C0", lw=.5)
    ax_a_dos.set_xlim(0)
    ax_a_dos.set_ylim(ymin, ymax)

    enk = Ry*1e3*loadtxt('./data/M27N26.rlx.enk_band.dat')
    nk, nb = shape(enk)
    ib1 = 14
    ib2 = 17
    for ib in range(nb):
        if ib1<=ib and ib<=ib2:
            ax_b_band.plot(enk[:,ib], '-', c="C3", lw=.5)
        else:
            ax_b_band.plot(enk[:,ib], '-', c="C3", lw=.5)

    ax_b_band.plot([0,90],[1.74,1.74], '--', c='C3', lw=.5)

    ax_b_band.grid(True, axis='x', **gridlinespec)
    ax_b_band.axhline(0,  **gridlinespec)
    ax_b_band.set_ylabel('')
    ax_b_band.set_ylim(ymin, ymax)
    ax_b_band.set_xticklabels(["$\Gamma$", "$K$", "$M$", "$\Gamma$"])
    ax_b_band.set_xticks([0,30,60,90])
    ax_b_band.set_xlim(0,90)
    ax_b_band.set_title('Relaxed', fontsize=8)
    ax_b_band.text(0.5, 0.75, '$\\theta=1.25^\circ$',
            transform=ax_b_band.transAxes, fontsize=7,
            fontweight='normal', va='center', ha='center',
            color="k")

    ax_b_dos.set_xticks([])
    ax_b_dos.set_yticks([])
    ax_b_dos.set_xlabel('')
    ax_b_dos.set_ylabel('')
    # ax_b_dos.set_xlabel('DOS', va='center', ha='center')
    # ax_b_dos.xaxis.set_label_coords(0.5, 1.08)
    ax_b_dos.set_title('DOS', fontsize=7)

    dos = loadtxt('./data/M27N26.rlx.edos.dat')
    ax_b_dos.plot(dos[:,1]/1e3, 1e3*dos[:,0], '-', c="C3", lw=.5)
    ax_b_dos.set_xlim(0)
    ax_b_dos.set_ylim(ymin, ymax)

    bw_nonrlx = loadtxt('./data/bandwidths_nonrlx.dat')
    bw_rlx = loadtxt('./data/bandwidths_rlx.dat')

    ax_c.plot(bw_nonrlx[:,0], bw_nonrlx[:,1], '.-', label='Nonrelaxed', c='C0')
    ax_c.plot(bw_rlx[:,0], bw_rlx[:,1], '.-', label='Relaxed', c='C3')
    ax_c.legend(frameon=False, fontsize=7., loc='best')
    ax_c.set_ylim(10,30)
    ax_c.set_xticks([1.0, 1.1, 1.2, 1.3, 1.4,1.5])
    ax_c.set_xlim(1.0, 1.5)
    ax_c.set_ylabel('Bandwidth (meV)')
    ax_c.set_xlabel('$\\theta$ (deg.)')

    # (d) Fermi surfaces at the half-filling of the electron-side
    M = 27
    N = M-1
    a = 2.46
    latt, relatt = generate_commensurate_cell(M, N, a)
    enk = 13.605e3*loadtxt('./data/M27N26.enk.dat')
    equiv = loadtxt('./data/kpoints_equiv.dat', skiprows=1)
    nk, nb = shape(enk)
    nk1 = 30
    nk2 = 30

    lrepeat = 2
    nrepeat = 2*lrepeat+1
    kx = zeros((nrepeat*nk1,nrepeat*nk2))
    ky = zeros((nrepeat*nk1,nrepeat*nk2))
    enk_grid = zeros((nb,nrepeat*nk1,nrepeat*nk2))

    for n1 in range(-lrepeat,lrepeat+1):
        for n2 in range(-lrepeat,lrepeat+1):
            for ik1 in range(nk1):
                for ik2 in range(nk2):
                    x1 = float(ik1)/nk1
                    x2 = float(ik2)/nk2
                    k = (n1+x1)*relatt[0,0:2]+(n2+x2)*relatt[1,0:2]
                    ii = (n1+lrepeat)*nk1+ik1
                    jj = (n2+lrepeat)*nk2+ik2
                    kx[ii,jj] = k[0]
                    ky[ii,jj] = k[1]

                    ik = ik1*nk1 + ik2

                    ik_irr = int(equiv[ik,2])-1

                    for ib in range(nb):
                        enk_grid[ib,ii,jj] = enk[ik_irr, ib]

# 1.73999999999999
# -4.74
    ecut = 1.74 # meV
    for ib in range(nb):
        ek = enk_grid[ib,:,:]
        if ek.min()>ecut or ek.max()<ecut:
            continue
        ax_d.contour(kx, ky, ek, levels=[ecut], colors='C3', \
                linestyles='-', linewidths=0.5)

    fac = 0.9
    ax_d.set_xlim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_d.set_ylim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_d.set_xticks([])
    ax_d.set_yticks([])
    ax_d.set_ylabel('$k_y$')
    ax_d.set_xlabel('$k_x$')

    # BZ
    plot_patch(ax_d, [
         relatt.T.dot([2./3., 1./3., 0.])[:2],
         relatt.T.dot([1./3., 2./3., 0.])[:2],
         relatt.T.dot([-1./3., 1./3., 0.])[:2],
        -relatt.T.dot([2./3., 1./3., 0.])[:2],
        -relatt.T.dot([1./3., 2./3., 0.])[:2],
        -relatt.T.dot([-1./3., 1./3., 0.])[:2],
         relatt.T.dot([2./3., 1./3., 0.])[:2],
        ],0.5,'k')

    G = relatt.T.dot([0., 0., 0.])[:2]
    M = relatt.T.dot([0.5, 0., 0.])[:2]
    K = relatt.T.dot([2./3., 1./3., 0.])[:2]
    vertices = [ G, M, K, G, ]
    ax_d.add_patch(patches.PathPatch(Path(vertices), \
            lw=.5, edgecolor='k', linestyle='--', fill=False))
    dx = 0.01*linalg.norm(K)
    ax_d.text(G[0]-15*dx, G[1]+10*dx, '$\Gamma$', \
            color='k', fontsize=8, va='center', ha='center')
    ax_d.text(K[0]+15*dx, K[1], '$K$', \
            color='k', fontsize=8, va='center', ha='center')
    ax_d.text(M[0]+15*dx, M[1]-15*dx, '$M$', \
            color='k', fontsize=8, va='center', ha='center')

    plt.savefig('figure2.eps', dpi=300)
    plt.savefig('figure2.pdf', dpi=300)
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
