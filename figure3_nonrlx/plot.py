from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches

# blue = '#203689'
# red = '#ba0004'
blue = 'C0'
red = 'C3'

def main():
    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 12/2.54   # 10 cm to inch
    fig = plt.figure(figsize=(fw,fh))

    gs = GridSpec(3,2,
            width_ratios=[1,1],
            height_ratios=[1,1,1],
            left=0.15,
            right=0.95,
            bottom=0.10,
            top=0.94,
            wspace=0.33,
            hspace=0.30)
    ax_a = plt.subplot(gs[0,0])
    ax_b = plt.subplot(gs[0,1])
    ax_c = plt.subplot(gs[1,0])
    ax_d = plt.subplot(gs[1,1])
    ax_e = plt.subplot(gs[2,0])
    ax_f = plt.subplot(gs[2,1])

    # subplot texts
    size = 8
    x = -.35
    y = 1.15
    ax_a.text(x, y, '(a)', transform=ax_a.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.15
    y = 1.15
    ax_b.text(x, y, '(b)', transform=ax_b.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.35
    y = 1.00
    ax_c.text(x, y, '(c)', transform=ax_c.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.15
    y = 1.00
    ax_d.text(x, y, '(d)', transform=ax_d.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.35
    y = 1.15
    ax_e.text(x, y, '(e)', transform=ax_e.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.15
    y = 1.15
    ax_f.text(x, y, '(f)', transform=ax_f.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")

    Ry = 13.605693
    Ry2meV = Ry*1e3
    ymin = -40
    ymax =  40
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }

    enk = Ry*1e3*loadtxt('./data/M27N26.Ez.0.000VPA.enk_band.dat')

    nk, nb = shape(enk)
    for ib in range(nb):
        ax_a.plot(enk[:,ib], '-', c=red, lw=.5)
    ax_a.grid(True, axis='x', **gridlinespec)
    ax_a.axhline(0,  **gridlinespec)
    ax_a.set_ylabel('Energy (meV)', ha='center', va='center', fontweight='bold')
    ax_a.set_ylim(ymin, ymax)
    ax_a.set_yticks([-40,-20,0,20,40])
    ax_a.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"],)
    ax_a.set_xticks([0,30,60,90])
    ax_a.set_xlim(0,90)
    ax_a.set_title('$\\bf{E_{z}}$ = 0 $\\bf{mV/\AA}$', fontsize=8, fontweight='bold')
    ax_a.plot([0,90],[1.74,1.74], '--', c=red, lw=.5)

    enk = Ry*1e3*loadtxt('./data/M27N26.Ez.0.005VPA.enk_band.dat')

    vb = 15
    vbm = enk[:,vb].max()

    nk, nb = shape(enk)
    for ib in range(nb):
        ax_b.plot(enk[:,ib]-vbm, '-', c=red, lw=.5)
    ax_b.grid(True, axis='x', **gridlinespec)
    ax_b.axhline(0,  **gridlinespec)
    # ax_b.set_ylabel('Energy (meV)', ha='center', va='center')
    ax_b.set_ylim(ymin, ymax)
    ax_b.set_yticks([-40,-20,0,20,40])
    ax_b.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"],)
    ax_b.set_xticks([0,30,60,90])
    ax_b.set_xlim(0,90)
    ax_b.set_title('$\\bf{E_{z}}$ = 5 $\\bf{mV/\AA}$', fontsize=8, fontweight='bold')
    ax_b.plot([0,90],[7.14-vbm,7.14-vbm], '--', c=red, lw=.5)

    dos = loadtxt('./data/M27N26.Ez.0.000VPA.layer_pdos.dat')
    y = dos[:,1]+dos[:,2]+dos[:,3]+dos[:,4]
    ax_e.fill_between(1e3*dos[:,0], 0, 2*y/1e3, linestyle='-', color=blue, lw=.5, label='Layer 4')
    y = dos[:,4]+dos[:,3]+dos[:,2]
    ax_e.fill_between(1e3*dos[:,0], 0, 2*y/1e3, linestyle='-', color='C1', lw=.5, label='Layer 3')
    y = dos[:,4]+dos[:,3]
    ax_e.fill_between(1e3*dos[:,0], 0, 2*y/1e3, linestyle='-', color='C2', lw=.5, label='Layer 2')
    y = dos[:,4]
    ax_e.fill_between(1e3*dos[:,0], 0, 2*y/1e3, linestyle='-', color=red, lw=.5, label='Layer 1')

    y = dos[:,1]+dos[:,2]+dos[:,3]+dos[:,4]
    ax_e.plot(1e3*dos[:,0], 2*y/1e3, '-', c='k', label='Total', lw=.5)

    handles, labels = ax_e.get_legend_handles_labels()
    h = [handles[0], handles[4], handles[3], handles[2], handles[1]]
    l = [labels[0], labels[4], labels[3], labels[2], labels[1]]
    ax_e.legend(h, l, frameon=False, fontsize=5, loc='upper right', handlelength=1)

    ax_e.set_xlim(-40,40)
    ax_e.set_ylim(0, 1.0)
    ax_e.set_xlabel('Energy (meV)', va='top', labelpad=-.09, fontweight='bold')
    ax_e.set_ylabel('DOS (states/meV)', fontsize=8, fontweight='bold')
    ax_e.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
    ax_e.set_xticks([-40,-20,0,20,40])

    dos = loadtxt('./data/M27N26.Ez.0.005VPA.layer_pdos.dat')

    vbm_dos = vbm
    y = dos[:,1]+dos[:,2]+dos[:,3]+dos[:,4]
    ax_f.fill_between(1e3*dos[:,0]-vbm_dos, 0, 2*y/1e3, linestyle='-', color=blue, lw=.5, label='Layer 4')
    y = dos[:,4]+dos[:,3]+dos[:,2]
    ax_f.fill_between(1e3*dos[:,0]-vbm_dos, 0, 2*y/1e3, linestyle='-', color='C1', lw=.5, label='Layer 3')
    y = dos[:,4]+dos[:,3]
    ax_f.fill_between(1e3*dos[:,0]-vbm_dos, 0, 2*y/1e3, linestyle='-', color='C2', lw=.5, label='Layer 2')
    y = dos[:,4]
    ax_f.fill_between(1e3*dos[:,0]-vbm_dos, 0, 2*y/1e3, linestyle='-', color=red, lw=.5, label='Layer 1')
    y = dos[:,1]+dos[:,2]+dos[:,3]+dos[:,4]
    ax_f.plot(1e3*dos[:,0]-vbm_dos, 2*y/1e3, '-', c='k', label='Total', lw=.5)

    # handles, labels = ax_f.get_legend_handles_labels()
    # ax_f.legend(handles, labels, frameon=False, fontsize=6, loc='upper left', handlelength=1)

    ax_f.set_xlim(-40,40)
    ax_f.set_ylim(0, 1.0)
    ax_f.set_xlabel('Energy (meV)', va='top', labelpad=-.09, fontweight='bold')
    ax_f.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
    ax_f.set_xticks([-40,-20,0,20,40])

    # (c) Fermi surfaces at the half-filling of the electron-side
    M = 27
    N = M-1
    a = 2.46
    latt, relatt = generate_commensurate_cell(M, N, a)
    enk1 = 13.605e3*loadtxt('./data/M27N26.Ez.0.000VPA.enk.dat')
    equiv = loadtxt('./data/kpoints_equiv.dat', skiprows=1)
    nk, nb = shape(enk1)
    nk1 = 30
    nk2 = 30

    lrepeat = 2
    nrepeat = 2*lrepeat+1
    kx = zeros((nrepeat*nk1,nrepeat*nk2))
    ky = zeros((nrepeat*nk1,nrepeat*nk2))
    enk_grid1 = zeros((nb,nrepeat*nk1,nrepeat*nk2))

    for n1 in range(-lrepeat,lrepeat+1):
        for n2 in range(-lrepeat,lrepeat+1):
            for ik1 in range(nk1):
                for ik2 in range(nk2):
                    x1 = float(ik1)/nk1
                    x2 = float(ik2)/nk2
                    k = (n1+x1)*relatt[0,0:2]+(n2+x2)*relatt[1,0:2]
                    ii = (n1+lrepeat)*nk1+ik1
                    jj = (n2+lrepeat)*nk2+ik2
                    kx[ii,jj] = k[0]
                    ky[ii,jj] = k[1]

                    ik = ik1*nk1 + ik2

                    ik_irr = int(equiv[ik,2])-1

                    for ib in range(nb):
                        enk_grid1[ib,ii,jj] = enk1[ik_irr, ib]
    ecut = 3.00
    for ib in range(nb):
        ek = enk_grid1[ib,:,:]
        if ek.min()>ecut or ek.max()<ecut:
            continue
        ax_c.contour(kx, ky, ek, levels=[ecut], colors=red, \
                linestyles='-', linewidths=0.5)

    fac = 0.9
    ax_c.set_xlim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_c.set_ylim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_c.set_xticks([])
    ax_c.set_yticks([])
    ax_c.set_ylabel('$k_y$', fontweight='bold')
    ax_c.set_xlabel('$k_x$', fontweight='bold')

    # BZ
    plot_patch(ax_c, [
         relatt.T.dot([2./3., 1./3., 0.])[:2],
         relatt.T.dot([1./3., 2./3., 0.])[:2],
         relatt.T.dot([-1./3., 1./3., 0.])[:2],
        -relatt.T.dot([2./3., 1./3., 0.])[:2],
        -relatt.T.dot([1./3., 2./3., 0.])[:2],
        -relatt.T.dot([-1./3., 1./3., 0.])[:2],
         relatt.T.dot([2./3., 1./3., 0.])[:2],
        ],0.5,'k')

    G = relatt.T.dot([0./3., 0./3., 0.])[:2]
    K = relatt.T.dot([2./3., 1./3., 0.])[:2]
    M = relatt.T.dot([1./2., 1./2., 0.])[:2]
    vertices = [ G, K, M, G, ]
    ax_c.add_patch(patches.PathPatch(Path(vertices), lw=.5, edgecolor='k', linestyle='--', fill=False))
    ax_d.add_patch(patches.PathPatch(Path(vertices), lw=.5, edgecolor='k', linestyle='--', fill=False))

    ax_c.text(G[0]-.007, G[1]-.007, '$\Gamma_s$',
      fontsize=6, va='center', ha='center', color="k")
    ax_d.text(G[0]-.007, G[1]-.007, '$\Gamma_s$',
      fontsize=6, va='center', ha='center', color="k")

    ax_c.text(K[0]+.005, K[1], '$K_s$',
      fontsize=6, va='center', ha='center', color="k")
    ax_d.text(K[0]+.005, K[1], '$K_s$',
      fontsize=6, va='center', ha='center', color="k")

    ax_c.text(M[0]+.005, M[1]+.002, '$M_s$',
      fontsize=6, va='center', ha='center', color="k")
    ax_d.text(M[0]+.005, M[1]+.002, '$M_s$',
      fontsize=6, va='center', ha='center', color="k")

    enk2 = 13.605e3*loadtxt('./data/M27N26.Ez.0.005VPA.enk.dat')
    equiv = loadtxt('./data/kpoints_equiv.dat', skiprows=1)
    nk, nb = shape(enk2)
    nk1 = 30
    nk2 = 30

    lrepeat = 2
    nrepeat = 2*lrepeat+1
    kx = zeros((nrepeat*nk1,nrepeat*nk2))
    ky = zeros((nrepeat*nk1,nrepeat*nk2))
    enk_grid2 = zeros((nb,nrepeat*nk1,nrepeat*nk2))

    for n1 in range(-lrepeat,lrepeat+1):
        for n2 in range(-lrepeat,lrepeat+1):
            for ik1 in range(nk1):
                for ik2 in range(nk2):
                    x1 = float(ik1)/nk1
                    x2 = float(ik2)/nk2
                    k = (n1+x1)*relatt[0,0:2]+(n2+x2)*relatt[1,0:2]
                    ii = (n1+lrepeat)*nk1+ik1
                    jj = (n2+lrepeat)*nk2+ik2
                    kx[ii,jj] = k[0]
                    ky[ii,jj] = k[1]

                    ik = ik1*nk1 + ik2

                    ik_irr = int(equiv[ik,2])-1

                    for ib in range(nb):
                        enk_grid2[ib,ii,jj] = enk2[ik_irr, ib]

    ecut = 7.14-vbm #  5.74 # meV
    for ib in range(nb):
        ek = enk_grid2[ib,:,:]-vbm
        if ek.min()>ecut or ek.max()<ecut:
            continue
        ax_d.contour(kx, ky, ek, levels=[ecut], colors=red, \
                linestyles='-', linewidths=0.5)

    fac = 0.9
    ax_d.set_xlim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_d.set_ylim(-fac*relatt[0,0],fac*relatt[0,0])
    ax_d.set_xticks([])
    ax_d.set_yticks([])
    ax_d.set_ylabel('$k_y$', fontweight='bold')
    ax_d.set_xlabel('$k_x$', fontweight='bold')

    # BZ
    plot_patch(ax_d, [
         relatt.T.dot([2./3., 1./3., 0.])[:2],
         relatt.T.dot([1./3., 2./3., 0.])[:2],
         relatt.T.dot([-1./3., 1./3., 0.])[:2],
        -relatt.T.dot([2./3., 1./3., 0.])[:2],
        -relatt.T.dot([1./3., 2./3., 0.])[:2],
        -relatt.T.dot([-1./3., 1./3., 0.])[:2],
         relatt.T.dot([2./3., 1./3., 0.])[:2],
        ],0.5,'k')

    plt.savefig('figure3.eps', dpi=300)
    plt.savefig('figure3.pdf', dpi=300)
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
