from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches

def main():
    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 12.0/2.54   # 10 cm to inch
    fig = plt.figure(figsize=(fw,fh))

    gs = GridSpec(3,7,
            width_ratios=[1,1,1,1.3,1,1,1])
    ax_a_band = plt.subplot(gs[0,:2])
    ax_a_dos  = plt.subplot(gs[0,2])
    ax_b_band = plt.subplot(gs[0,4:6])
    ax_b_dos  = plt.subplot(gs[0,6])
    ax_c_band = plt.subplot(gs[1,:2])
    ax_c_dos  = plt.subplot(gs[1,2])
    ax_d_band = plt.subplot(gs[1,4:6])
    ax_d_dos  = plt.subplot(gs[1,6])
    ax_e_band = plt.subplot(gs[2,:2])
    ax_e_dos  = plt.subplot(gs[2,2])
    ax_f_band = plt.subplot(gs[2,4:6])
    ax_f_dos  = plt.subplot(gs[2,6])

    left  = 0.18   # the left side of the subplots of the figure
    right = 0.96   # the right side of the subplots of the figure
    bottom = 0.05   # the bottom of the subplots of the figure
    top = 0.92     # the top of the subplots of the figure
    wspace = 0.00  # the amount of width reserved for blank space between subplots
    hspace = 0.45   # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left,
                        bottom=bottom,
                        right=right,
                        top=top,
                        wspace=wspace,
                        hspace=hspace)

    # subplot texts
    size=10
    x = -.7
    y = 1.20
    ax_a_band.text(x, y, '(a)', transform=ax_a_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.4
    ax_b_band.text(x, y, '(b)', transform=ax_b_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.7
    y = 1.15
    ax_c_band.text(x, y, '(c)', transform=ax_c_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.4
    ax_d_band.text(x, y, '(d)', transform=ax_d_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.7
    y = 1.15
    ax_e_band.text(x, y, '(e)', transform=ax_e_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")
    x = -.4
    y = 1.15
    ax_f_band.text(x, y, '(f)', transform=ax_f_band.transAxes,
      fontsize=size, fontweight='normal', va='center', ha='center', color="k")

    Ry = 13.605693
    Ry2meV = Ry*1e3
    ymin = -200
    ymax =  200
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }
    enk = Ry*1e3*loadtxt('./data/M11N10.nonrlx.TB.enk_band.dat')
    vb = 15
    vbm = enk[:,vb].max()
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_a_band.plot(enk[:,ib]-vbm, '-', c="C0", lw=.5)
    ax_a_band.grid(True, axis='x', **gridlinespec)
    ax_a_band.axhline(0,  **gridlinespec)
    ax_a_band.set_ylabel('Energy (meV)', ha='center', va='center', fontweight='bold')
    # ax_a_band.yaxis.set_label_coords(-0.55, 0.5)
    ax_a_band.set_ylim(ymin, ymax)
    ax_a_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_a_band.set_xticks([0,30,60,90])
    ax_a_band.set_xlim(0,90)
    ax_a_band.set_title('Nonrelaxed\n$\\theta=3.15^\circ$', fontsize=8, fontweight='bold')
    # ax_a_dos.text(0.90, 0.95, '$3.15^\circ$',
    #         transform=ax_a_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_a_dos.set_xticks([])
    ax_a_dos.set_yticks([])
    ax_a_dos.set_ylabel('')
    # ax_a_dos.set_title('DOS', fontsize=7, fontweight='bold')

    dos = loadtxt('./data/M11N10.nonrlx.TB.edos.dat')
    M = 11
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_a_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0]-vbm, '-', c="C0", lw=.5)
    ax_a_dos.set_xlim(0)
    ax_a_dos.set_ylim(ymin, ymax)

    enk = Ry*1e3*loadtxt('./data/M11N10.rlx.TB.enk_band.dat')
    vb = 15
    vbm = enk[:,vb].max()
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_b_band.plot(enk[:,ib]-vbm, '-', c="C3", lw=.5)

    ax_b_band.grid(True, axis='x', **gridlinespec)
    ax_b_band.axhline(0,  **gridlinespec)
    ax_b_band.set_ylabel('')
    ax_b_band.set_ylim(ymin, ymax)
    ax_b_band.set_xticklabels(["$\Gamma$", "$K$", "$M$", "$\Gamma$"])
    ax_b_band.set_xticks([0,30,60,90])
    ax_b_band.set_xlim(0,90)
    ax_b_band.set_title('Relaxed\n$\\theta=3.15^\circ$', fontsize=8, fontweight='bold')
    # ax_b_dos.text(0.90, 0.95, '$3.15^\circ$',
    #         transform=ax_b_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_b_dos.set_xticks([])
    ax_b_dos.set_yticks([])
    ax_b_dos.set_xlabel('')
    ax_b_dos.set_ylabel('')
    # ax_b_dos.set_title('DOS', fontsize=7, fontweight='bold')

    dos = loadtxt('./data/M11N10.rlx.TB.edos.dat')
    M = 11
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_b_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c="C3", lw=.5)
    ax_b_dos.set_xlim(0)
    ax_b_dos.set_ylim(ymin, ymax)

    # Figure (c)
    ymin = -100
    ymax =  100
    enk = Ry*1e3*loadtxt('./data/M19N18.nonrlx.enk_band.dat')
    vb = 15
    vbm = enk[:,vb].max()
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_c_band.plot(enk[:,ib]-vbm, '-', c="C0", lw=.5)
    ax_c_band.grid(True, axis='x', **gridlinespec)
    ax_c_band.axhline(0,  **gridlinespec)
    ax_c_band.set_ylabel('Energy (meV)', ha='center', va='center',
            fontweight='bold')
    # ax_c_band.yaxis.set_label_coords(-0.55, 0.5)
    ax_c_band.set_ylim(ymin, ymax)
    ax_c_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_c_band.set_xticks([0,30,60,90])
    ax_c_band.set_xlim(0,90)
    ax_c_band.set_title('$\\theta=1.79^\circ$', fontsize=8, fontweight='bold')
    # ax_c_band.set_title('Nonrelaxed', fontsize=8)
    # ax_c_dos.text(0.90, 0.95, '$1.79^\circ$',
    #         transform=ax_c_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_c_dos.set_xticks([])
    ax_c_dos.set_yticks([])
    ax_c_dos.set_ylabel('')
    # ax_c_dos.set_title('DOS', fontsize=7)

    dos = loadtxt('./data/M19N18.nonrlx.edos.dat')
    M = 19
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_c_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0]-vbm, '-', c="C0", lw=.5)
    ax_c_dos.set_xlim(0)
    ax_c_dos.set_ylim(ymin, ymax)

    enk = Ry*1e3*loadtxt('./data/M19N18.rlx.enk_band.dat')
    vb = 15
    vbm = enk[:,vb].max()
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_d_band.plot(enk[:,ib]-vbm, '-', c="C3", lw=.5)

    ax_d_band.grid(True, axis='x', **gridlinespec)
    ax_d_band.axhline(0,  **gridlinespec)
    ax_d_band.set_ylabel('')
    ax_d_band.set_ylim(ymin, ymax)
    ax_d_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_d_band.set_xticks([0,30,60,90])
    ax_d_band.set_xlim(0,90)
    # ax_d_band.set_title('Relaxed', fontsize=8)
    ax_d_band.set_title('$\\theta=1.79^\circ$', fontsize=8, fontweight='bold')
    # ax_d_dos.text(0.90, 0.95, '$1.79^\circ$',
    #         transform=ax_d_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_d_dos.set_xticks([])
    ax_d_dos.set_yticks([])
    ax_d_dos.set_xlabel('')
    ax_d_dos.set_ylabel('')
    # ax_d_dos.set_title('DOS', fontsize=7)

    dos = loadtxt('./data/M19N18.rlx.edos.dat')
    M = 19
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_d_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0]-vbm, '-', c="C3", lw=.5)
    ax_d_dos.set_xlim(0)
    ax_d_dos.set_ylim(ymin, ymax)

    enk = Ry*1e3*loadtxt('./data/M27N26.nonrlx.enk_band.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_e_band.plot(enk[:,ib], '-', c="C0", lw=.5)
    ax_e_band.grid(True, axis='x', **gridlinespec)
    ax_e_band.axhline(0,  **gridlinespec)
    ax_e_band.set_ylabel('Energy (meV)', ha='center', va='center', fontweight='bold')
    # ax_e_band.yaxis.set_label_coords(-0.55, 0.5)
    ax_e_band.set_ylim(ymin, ymax)
    ax_e_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_e_band.set_xticks([0,30,60,90])
    ax_e_band.set_xlim(0,90)
    ax_e_band.set_title('$\\theta=1.25^\circ$', fontsize=8, fontweight='bold')
    # ax_e_dos.text(0.90, 0.95, '$1.25^\circ$',
    #         transform=ax_e_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_e_dos.set_xticks([])
    ax_e_dos.set_yticks([])
    ax_e_dos.set_ylabel('')

    dos = loadtxt('./data/M27N26.nonrlx.edos.dat')
    M = 27
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_e_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c="C0", lw=.5)
    ax_e_dos.set_xlim(0)
    ax_e_dos.set_ylim(ymin, ymax)

    enk = Ry*1e3*loadtxt('./data/M27N26.rlx.enk_band.dat')
    nk, nb = shape(enk)
    ib1 = 14
    ib2 = 17
    for ib in range(nb):
        if ib1<=ib and ib<=ib2:
            ax_f_band.plot(enk[:,ib], '-', c="C3", lw=.5)
        else:
            ax_f_band.plot(enk[:,ib], '-', c="C3", lw=.5)

    ax_f_band.grid(True, axis='x', **gridlinespec)
    ax_f_band.axhline(0,  **gridlinespec)
    ax_f_band.set_ylabel('')
    ax_f_band.set_ylim(ymin, ymax)
    ax_f_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_f_band.set_xticks([0,30,60,90])
    ax_f_band.set_xlim(0,90)
    ax_f_band.set_title('$\\theta=1.25^\circ$', fontsize=8, fontweight='bold')
    # ax_f_dos.text(0.90, 0.95, '$1.25^\circ$',
    #         transform=ax_f_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_f_dos.set_xticks([])
    ax_f_dos.set_yticks([])
    ax_f_dos.set_xlabel('')
    ax_f_dos.set_ylabel('')

    dos = loadtxt('./data/M27N26.rlx.edos.dat')
    M = 27
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)
    ax_f_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c="C3", lw=.5)
    ax_f_dos.set_xlim(0)
    ax_f_dos.set_ylim(ymin, ymax)

    xmax = 0.005
    ax_a_dos.set_xlim(0,xmax)
    ax_b_dos.set_xlim(0,xmax)
    ax_c_dos.set_xlim(0,xmax)
    ax_d_dos.set_xlim(0,xmax)
    ax_e_dos.set_xlim(0,xmax)
    ax_f_dos.set_xlim(0,xmax)

    plt.savefig('s2.eps', dpi=300)
    plt.savefig('s2.pdf', dpi=300)
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
