from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import cm

# blue = '#353b8f'
# red = '#b61342'
# blue = '#203689'
# red = '#ba0004'
blue = 'C0'
red = 'C3'
draw_nonrlx = False

def main():

    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 10.0/2.54   # 10 cm to inch

    colors = ['lightgreen', 'orange', red]
    # cmap = cm.get_cmap('bwr', 2)    # PiYG
    # for i in range(cmap.N):
    #     rgb = cmap(i)[:3] # will return rgba, we take only first 3 so we get rgb
    #     colors.append(matplotlib.colors.rgb2hex(rgb))

    fig = plt.figure(figsize=(fw,fh))

    gs = GridSpec(2,7,
            width_ratios=[1,1,1,1.35,1,1,1])
    ax_a_band = plt.subplot(gs[0,:2])
    ax_a_dos  = plt.subplot(gs[0,2])
    ax_b_band = plt.subplot(gs[0,4:6])
    ax_b_dos  = plt.subplot(gs[0,6])
    ax_c_band = plt.subplot(gs[1,:2])
    ax_c_dos  = plt.subplot(gs[1,2])
    ax_d_band = plt.subplot(gs[1,4:6])
    ax_d_dos  = plt.subplot(gs[1,6])

    left  = 0.18   # the left side of the subplots of the figure
    right = 0.96   # the right side of the subplots of the figure
    bottom = 0.07   # the bottom of the subplots of the figure
    top = 0.90     # the top of the subplots of the figure
    wspace = 0.00  # the amount of width reserved for blank space between subplots
    hspace = 0.35  # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left,
                        bottom=bottom,
                        right=right,
                        top=top,
                        wspace=wspace,
                        hspace=hspace)

    # subplot texts
    size=8
    x = -.73
    y = 1.20
    ax_a_band.text(x, y, '(a)', transform=ax_a_band.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.50
    ax_b_band.text(x, y, '(b)', transform=ax_b_band.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.73
    y = 1.12
    ax_c_band.text(x, y, '(c)', transform=ax_c_band.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")
    x = -.50
    ax_d_band.text(x, y, '(d)', transform=ax_d_band.transAxes,
      fontsize=size, fontweight='bold', va='center', ha='center', color="k")

    Ry = 13.605693
    Ry2meV = Ry*1e3
    ymin = -200
    ymax =  200
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }

    #############
    ## Fig (a) ##
    #############
    if draw_nonrlx:
        enk = Ry*1e3*loadtxt('./data/TBG.M19N18.nonrlx.enk_band.dat')
        nk, nb = shape(enk)
        for ib in range(nb):
            ax_a_band.plot(enk[:,ib], '--', c="k", lw=.2)

    enk = Ry*1e3*loadtxt('./data/TBG.M19N18.rlx.enk_band.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_a_band.plot(enk[:,ib], '-', c=blue, lw=.5)

    ax_a_band.grid(True, axis='x', **gridlinespec)
    ax_a_band.axhline(0,  **gridlinespec)
    ax_a_band.set_ylabel('Energy (meV)', fontweight='bold')
    ax_a_band.set_ylim(ymin, ymax)
    ax_a_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_a_band.set_xticks([0,30,60,90])
    ax_a_band.set_xlim(0,90)
    ax_a_band.set_title('TBG\n$\\theta=1.79^\circ$',
            fontsize=8, fontweight='bold')
    # ax_a_dos.text(1.00, 0.96, '$1.79^\circ$',
    #         transform=ax_a_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_a_dos.set_xticks([])
    ax_a_dos.set_yticks([])
    ax_a_dos.set_xlabel('')
    ax_a_dos.set_ylabel('')
    ax_a_dos.set_title('DOS', fontsize=7, fontweight='bold')
    ax_b_dos.set_title('DOS', fontsize=7, fontweight='bold')
    ax_c_dos.set_title('DOS', fontsize=7, fontweight='bold')
    ax_d_dos.set_title('DOS', fontsize=7, fontweight='bold')

    M = 19
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)

    if draw_nonrlx:
        dos = loadtxt('./data/TBG.M19N18.nonrlx.edos.dat')
        ax_a_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '--', c="k", lw=.2)
    dos = loadtxt('./data/TBG.M19N18.rlx.edos.dat')
    ax_a_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c=blue, lw=.5)

    ax_a_dos.set_xlim(0,0.003)
    ax_a_dos.set_ylim(ymin, ymax)

    #############
    ## Fig (b) ##
    #############


    M = 19
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)

    if draw_nonrlx:
        enk = Ry*1e3*loadtxt('./data/M19N18.nonrlx.enk_band.dat')
        vb = 15
        vbm = enk[:,vb].max()
        nk, nb = shape(enk)
        for ib in range(nb):
            ax_b_band.plot(enk[:,ib]-vbm, '--', c="k", lw=.2)
        dos = loadtxt('./data/M19N18.nonrlx.edos.dat')
        ax_b_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0]-vbm, '--', c="k", lw=.2)

    enk = Ry*1e3*loadtxt('./data/M19N18.rlx.enk_band.dat')
    vb = 15
    vbm = enk[:,vb].max()
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_b_band.plot(enk[:,ib]-vbm, '-', c=red, lw=.5)
    dos = loadtxt('./data/M19N18.rlx.edos.dat')
    ax_b_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0]-vbm, '-', c=red, lw=.5)


    ax_b_band.grid(True, axis='x', **gridlinespec)
    ax_b_band.axhline(0,  **gridlinespec)
    ax_b_band.set_ylabel('')
    ax_b_band.set_ylim(ymin, ymax)
    ax_b_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_b_band.set_xticks([0,30,60,90])
    ax_b_band.set_xlim(0,90)
    ax_b_band.set_title('TDBG\n$\\theta=1.79^\circ$', fontsize=8, fontweight='bold')
    # ax_b_dos.text(1.00, 0.96, '',
    #         transform=ax_b_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_b_dos.set_xticks([])
    ax_b_dos.set_yticks([])
    ax_b_dos.set_xlabel('')
    ax_b_dos.set_ylabel('')
    # ax_b_dos.set_title('DOS', fontsize=7, fontweight='bold')

    ax_b_dos.set_xlim(0,0.003)
    ax_b_dos.set_ylim(ymin, ymax)

    # vb = 15
    # e1 = enk[:,vb+1].min()-vbm
    # e2 = enk[:,vb].max()-vbm
    # vertices = [
    #         [0,e1],
    #         [90,e1],
    #         [90,e2],
    #         [0,e2],
    #         [0,e1],
    #         ]
    # ax_b_band.add_patch(patches.PathPatch(Path(vertices), \
    #         lw=0, edgecolor='none', facecolor=colors[2], \
    #         fill=True, alpha=0.2))

    #############
    ## Fig (c) ##
    #############
    if draw_nonrlx:
        enk = Ry*1e3*loadtxt('./data/TBG.M27N26.nonrlx.enk_band.dat')
        nk, nb = shape(enk)
        for ib in range(nb):
            ax_c_band.plot(enk[:,ib], '--', c="k", lw=.2)

    enk = Ry*1e3*loadtxt('./data/TBG.M27N26.rlx.enk_band.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_c_band.plot(enk[:,ib], '-', c=blue, lw=.5)

    ax_c_band.grid(True, axis='x', **gridlinespec)
    ax_c_band.axhline(0,  **gridlinespec)
    ax_c_band.set_ylabel('Energy (meV)', fontweight='bold')
    ax_c_band.set_ylim(ymin, ymax)
    ax_c_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_c_band.set_xticks([0,30,60,90])
    ax_c_band.set_xlim(0,90)
    ax_c_band.set_title('$\\theta=1.25^\circ$', fontsize=8)
    # ax_c_dos.text(1.00, 0.96, '$1.25^\circ$',
    #         transform=ax_c_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_c_dos.set_xticks([])
    ax_c_dos.set_yticks([])
    ax_c_dos.set_xlabel('')
    ax_c_dos.set_ylabel('')

    M = 27
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)

    if draw_nonrlx:
        dos = loadtxt('./data/TBG.M27N26.nonrlx.edos.dat')
        ax_c_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '--', c="k", lw=.2)
    dos = loadtxt('./data/TBG.M27N26.rlx.edos.dat')
    ax_c_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c=blue, lw=.5)

    ax_c_dos.set_xlim(0, 0.003)
    ax_c_dos.set_ylim(ymin, ymax)

    #############
    ## Fig (c) ##
    #############

    if draw_nonrlx:
        enk = Ry*1e3*loadtxt('./data/M27N26.nonrlx.enk_band.dat')
        nk, nb = shape(enk)
        for ib in range(nb):
            ax_d_band.plot(enk[:,ib], '--', c="k", lw=.2)

    enk = Ry*1e3*loadtxt('./data/M27N26.rlx.enk_band.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        ax_d_band.plot(enk[:,ib], '-', c=red, lw=.5)

    ax_d_band.grid(True, axis='x', **gridlinespec)
    ax_d_band.axhline(0,  **gridlinespec)
    ax_d_band.set_ylabel('')
    ax_d_band.set_ylim(ymin, ymax)
    ax_d_band.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_d_band.set_xticks([0,30,60,90])
    ax_d_band.set_xlim(0,90)
    ax_d_band.set_title('$\\theta=1.25^\circ$', fontsize=8)
    # ax_d_band.set_title('Relaxed', fontsize=8)
    # ax_d_band.text(0.5, 0.75, '$\\theta=1.25^\circ$',
    #         transform=ax_d_band.transAxes, fontsize=7,
    #         fontweight='normal', va='center', ha='center',
    #         color="k")
    # ax_d_dos.text(1.00, 0.96, '$1.25^\circ$',
    #         transform=ax_d_dos.transAxes, fontsize=7,
    #         fontweight='normal', va='top', ha='right',
    #         color="k")

    ax_d_dos.set_xticks([])
    ax_d_dos.set_yticks([])
    ax_d_dos.set_xlabel('')
    ax_d_dos.set_ylabel('')
    # ax_d_dos.set_xlabel('DOS', va='center', ha='center')
    # ax_d_dos.xaxis.set_label_coords(0.5, 1.08)
    # ax_d_dos.set_title('DOS', fontsize=7)

    M = 27
    N = M-1
    A_cell = .246*.246*sqrt(3)/2 * (N*N+N*M+M*M)

    if draw_nonrlx:
        dos = loadtxt('./data/M27N26.nonrlx.edos.dat')
        ax_d_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '--', c="k", lw=.2)

    dos = loadtxt('./data/M27N26.rlx.edos.dat')
    ax_d_dos.plot(dos[:,1]/1e3/A_cell, 1e3*dos[:,0], '-', c=red, lw=.5)

    ax_d_dos.set_xlim(0,0.003)
    ax_d_dos.set_ylim(ymin, ymax)

    # vb = 14
    # e1 = enk[:,vb].min()
    # e2 = enk[:,vb-1].max()
    # vertices = [
    #         [0,e1],
    #         [90,e1],
    #         [90,e2],
    #         [0,e2],
    #         [0,e1],
    #         ]
    # ax_d_band.add_patch(patches.PathPatch(Path(vertices), \
    #         lw=0, edgecolor='none', facecolor=colors[0], \
    #         fill=True, alpha=0.2))
    # ax_d_band.text(45, (e1+e2)/2., '$\Delta_{\mathrm{s},h}$',
    #         fontsize=7, fontweight='normal', va='center', ha='center',
    #         color="k")

    # cb = 17
    # e1 = enk[:,cb+1].min()
    # e2 = enk[:,cb].max()
    # vertices = [
    #         [0,e1],
    #         [90,e1],
    #         [90,e2],
    #         [0,e2],
    #         [0,e1],
    #         ]
    # ax_d_band.add_patch(patches.PathPatch(Path(vertices), \
    #         lw=0, edgecolor='none', facecolor=colors[1], \
    #         fill=True, alpha=0.2))
    # ax_d_band.text(45, (e1+e2)/2., '$\Delta_{\mathrm{s},e}$',
    #         fontsize=7, fontweight='normal', va='center', ha='center',
    #         color="k")



    plt.savefig('s1.eps', dpi=300)
    plt.savefig('s1.pdf', dpi=300)
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
