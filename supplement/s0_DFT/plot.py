from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import cm

# blue = '#353b8f'
# red = '#b61342'
# blue = '#203689'
# red = '#ba0004'
blue = 'C0'
red = 'C3'

def main():

    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 8.6/2.54   # 10 cm to inch

    colors = ['lightgreen', 'orange', red]

    fig = plt.figure(figsize=(fw,fh))

    gs = GridSpec(1,2,
            width_ratios=[1,1])
    ax_a = plt.subplot(gs[0,0])
    ax_b = plt.subplot(gs[0,1])

    left  = 0.18   # the left side of the subplots of the figure
    right = 0.96   # the right side of the subplots of the figure
    bottom = 0.10   # the bottom of the subplots of the figure
    top = 0.87     # the top of the subplots of the figure
    wspace = 0.00  # the amount of width reserved for blank space between subplots
    hspace = 0.3   # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left,
                        bottom=bottom,
                        right=right,
                        top=top,
                        wspace=wspace,
                        hspace=hspace)


    Ry = 13.605693
    Ry2meV = Ry*1e3
    ymin = -.3
    ymax =  .3
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }

    #############
    ## Fig (a) ##
    #############
    enk = loadtxt('./data/M11N10.nonrlx.DFT.enk_band.dat')
    nb, nk = shape(enk)
    for ek in enk:
        ax_a.plot(arange(nk)/nk, ek, '--', c="k", lw=1)

    enk = loadtxt('./data/M11N10.rlx.DFT.enk_band.dat')
    nk, nb = shape(enk)
    nb, nk = shape(enk)
    for ek in enk:
        ax_a.plot(arange(nk)/nk, ek, '-', c="r", lw=1)

    ax_a.grid(True, axis='x', **gridlinespec)
    ax_a.axhline(0,  **gridlinespec)
    ax_a.set_ylabel('Energy (eV)', fontweight='bold')
    ax_a.set_ylim(ymin, ymax)
    ax_a.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_a.set_xticks([0,1/3,2/3,1])
    ax_a.set_xlim(0,1)
    ax_a.set_title('DFT (GGA)\n$\\theta=3.15^\circ$', fontsize=10, fontweight='bold')

    enk = Ry*loadtxt('./data/M11N10.nonrlx.TB.enk_band.dat').T
    nb, nk = shape(enk)
    for ek in enk:
        ax_b.plot(arange(nk)/nk, ek, '--', c="k", lw=1)

    enk = Ry*loadtxt('./data/M11N10.rlx.TB.enk_band.dat').T
    nk, nb = shape(enk)
    nb, nk = shape(enk)
    for ek in enk:
        ax_b.plot(arange(nk)/nk, ek, '-', c="r", lw=1)

    ax_b.grid(True, axis='x', **gridlinespec)
    ax_b.axhline(0,  **gridlinespec)
    ax_b.set_ylabel('')
    ax_b.set_yticks([])
    ax_b.set_ylim(ymin, ymax)
    ax_b.set_xticklabels(["$\Gamma_s$", "$K_s$", "$M_s$", "$\Gamma_s$"])
    ax_b.set_xticks([0,1/3,2/3,1])
    ax_b.set_xlim(0,1)
    ax_b.set_title('TB\n$\\theta=3.15^\circ$', fontsize=10, fontweight='bold')

    plt.savefig('s1.eps', dpi=300)
    plt.savefig('s1.pdf', dpi=300)
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
