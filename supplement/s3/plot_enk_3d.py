from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.path import Path
import matplotlib.patches as patches

def main():
    M = 27
    N = M-1
    a = 2.46
    latt, relatt = generate_commensurate_cell(M, N, a)
    enk = 13.605e3*loadtxt('./data/M27N26.Ez.0.005VPA.enk_nk120.dat')
    equiv = loadtxt('./data/kpoints_equiv_nk120.dat', skiprows=1)
    nk, nb = shape(enk)
    nk1 = 120
    nk2 = 120

    lrepeat = 0
    nrepeat = 2*lrepeat+1
    kx = zeros((nrepeat*nk1,nrepeat*nk2))
    ky = zeros((nrepeat*nk1,nrepeat*nk2))
    enk_grid = zeros((nb,nrepeat*nk1,nrepeat*nk2))

    for n1 in range(-lrepeat,lrepeat+1):
        for n2 in range(-lrepeat,lrepeat+1):
            for ik1 in range(nk1):
                for ik2 in range(nk2):
                    x1 = float(ik1)/nk1
                    x2 = float(ik2)/nk2
                    k = (n1+x1)*relatt[0,0:2]+(n2+x2)*relatt[1,0:2]
                    ii = (n1+lrepeat)*nk1+ik1
                    jj = (n2+lrepeat)*nk2+ik2
                    kx[ii,jj] = k[0]
                    ky[ii,jj] = k[1]

                    ik = ik1*nk1 + ik2

                    ik_irr = int(equiv[ik,2])-1

                    for ib in range(nb):
                        enk_grid[ib,ii,jj] = enk[ik_irr, ib]
    cb1 = 8428+1-8413
    cb2 = 8428+2-8413
    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(kx,ky,enk_grid[cb1,:,:])
    ax.plot_surface(kx,ky,enk_grid[cb2,:,:])
    fac = 0.9
    ax.set_zlim(0,30)
    ax.set_xlim(-fac*relatt[0,0],fac*relatt[0,0])
    ax.set_ylim(-fac*relatt[0,0],fac*relatt[0,0])
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data
def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
