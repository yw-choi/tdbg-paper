from matplotlib import rcParams
import matplotlib
# rcParams['font.family'] = 'Arial'
font = { 'size'   : 8}
matplotlib.rc('font', **font)
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import *
from matplotlib import gridspec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1 import AxesGrid
from matplotlib.patches import Polygon
quick = False
def main():
    dpi = 400
    fw = 8.6/2.54  # 8.6 cm to inch
    fh = 12./2.54   # 10 cm to inch
    fig = plt.figure(figsize=(fw,fh))
    gs = gridspec.GridSpec(4,2, width_ratios=[1,1],
                                height_ratios=[9,1.5,8,8])

    subplotspec = gs.new_subplotspec((0,0), 1, 1)
    ax_a = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((0,1), 1, 1)
    ax_b = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((1,0), 1, 2)
    ax_0 = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((2,0), 1, 1)
    ax_1 = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((2,1), 1, 1)
    ax_2 = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((3,0), 1, 1)
    ax_3 = plt.subplot(subplotspec)

    subplotspec = gs.new_subplotspec((3,1), 1, 1)
    ax_4 = plt.subplot(subplotspec)


    left  = 0.060  # the left side of the subplots of the figure
    right = 0.98   # the right side of the subplots of the figure
    bottom = 0.02  # the bottom of the subplots of the figure
    top = 1.03     # the top of the subplots of the figure
    wspace = 0.05  # the amount of width reserved for blank space between subplots
    hspace = 0.05  # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left,
                        bottom=bottom,
                        right=right,
                        top=top,
                        wspace=wspace,
                        hspace=hspace)

    # subplot texts
    size=10
    x = -0.13
    y = 0.90
    ax_a.text(x, y, '(a)', transform=ax_a.transAxes,
      fontsize=size, fontweight='bold', va='top', color="k")
    x = 1.00
    y = 0.90
    ax_b.text(x, y, '(b)', transform=ax_a.transAxes,
      fontsize=size, fontweight='bold', va='top', color="k")
    x = -0.13
    y =  0.20
    ax_4.text(x, y, '(c)', transform=ax_a.transAxes,
      fontsize=size, fontweight='bold', va='top', color="k")

    ax_a.set_axis_off()
    # img = mpimg.imread('data/top.png')
    # ax_a.imshow(img)

    ax_b.set_axis_off()
    # img = mpimg.imread('data/side.png')
    # ax_b.imshow(img)

    dz = 0.15
    topz = 0.80
    xx = 0.8
    ax_b.text(xx, topz, 'Layer 1', fontsize=8, va='center', ha='center', color='C0',
            transform=ax_b.transAxes, fontweight='bold')
    ax_b.text(xx, topz-dz, 'Layer 2', fontsize=8, va='center', ha='center', color='magenta',
            transform=ax_b.transAxes, fontweight='bold')
    ax_b.text(xx, topz-2*dz, 'Layer 3', fontsize=8, va='center', ha='center', color='C0',
            transform=ax_b.transAxes, fontweight='bold')
    ax_b.text(xx, topz-3*dz, 'Layer 4', fontsize=8, va='center', ha='center', color='magenta',
            transform=ax_b.transAxes, fontweight='bold')

    ### Figure 1-(a) M31N30 in-plance displacement
    # latt, data = read_atoms_out('./data/M31N30.atoms.out')
    latt, atoms_in = read_atoms_out('./data/M27N26.atoms.in')
    latt, atoms_out = read_atoms_out('./data/M27N26.atoms.out')
    N_LAYER = 4
    na = len(atoms_in)
    na_l = int(na/4)
    x = []
    y = []
    dx = []
    dz = []
    xmin, xmax = -60, 260
    ymin, ymax = -60, 260
    # xmin, xmax = -10, 10
    # ymin, ymax = -10, 10
    th = 1.248242/2 * pi / 180

    il = 2
    for n1 in range(-2,3):
        for n2 in range(-1,3):
            for ia in range(il*na_l,(il+1)*na_l):
                R = n1*latt[:,0]+n2*latt[:,1]
                xpos = R[0]+atoms_in[ia,0]
                ypos = R[1]+atoms_in[ia,1]
                if xmin-10<xpos and xpos<xmax+10 and ymin-10<ypos and ypos<ymax+10:
                    x.append(xpos)
                    y.append(ypos)
                    d1 = atoms_out[ia,0]-atoms_in[ia,0]
                    d2 = atoms_out[ia,1]-atoms_in[ia,1]
                    d3 = atoms_out[ia,2]-atoms_in[ia,2]
                    dx.append(sqrt(d1*d1+d2*d2))
                    dz.append(d3)
    x = array(x)
    y = array(y)
    dx = array(dx)
    dz = array(dz)

    dxmax = max(dx.max(),dz.max())
    dxmin = min(dx.min(),dz.min())
    mid_zero = abs(dxmin)/(dxmax-dxmin)
    cname = 'RdBu_r'
    # cname = 'jet'
    cmap = shiftedColorMap(plt.cm.get_cmap(cname), start=0, stop=1.0, \
            midpoint=mid_zero, name='shifted')
    # cmap = 'terrain'
    if not quick:
        qv = ax_3.tripcolor(x,y,dx, rasterized=True, vmin=dxmin, vmax=dxmax, cmap=cmap)

    ax_0.set_axis_off()
    # ax_0.set_ymargin(10)
    # if not quick:
    #     cbar = fig.colorbar(qv, cax=ax_0, orientation='horizontal')
    # ax_0.xaxis.set_tick_params(labelsize=8)
    # ax_0.xaxis.set_ticks_position("top")
    # ax_0.xaxis.set_label_position("top")
    # ax_0.set_xlabel("Displacement ($\AA$)", fontsize=8)
    divider = make_axes_locatable(ax_0)
    cax = divider.append_axes('top', size='60%')
    if not quick:
        cbar = fig.colorbar(qv, cax=cax, orientation='horizontal')

    cax.xaxis.set_tick_params(labelsize=8)
    cax.xaxis.set_ticks_position("top")
    cax.xaxis.set_label_position("top")
    cax.set_xlabel("Displacement ($\AA$)", fontsize=8, fontweight='bold')

    ax_3.set_xlim(xmin,xmax)
    ax_3.set_ylim(ymin,ymax)
    ax_3.get_xaxis().set_ticks([])
    ax_3.get_yaxis().set_ticks([])
    ax_3.set_ylabel('Layer 2', fontweight='bold')

    if not quick:
        qv = ax_4.tripcolor(x,y,dz, rasterized=True, vmin=dxmin, vmax=dxmax, cmap=cmap)

    ax_4.set_xlim(xmin,xmax)
    ax_4.set_ylim(ymin,ymax)
    ax_4.get_xaxis().set_ticks([])
    ax_4.get_yaxis().set_ticks([])

    # fc = "white"
    # ax_3.text(0,0, "AA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_4.text(0,0, "AA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # x = 1./3*latt[0,0]+1./3*latt[0,1]
    # y = 1./3*latt[1,0]+1./3*latt[1,1]
    # ax_3.text(x,y, "AB", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_4.text(x,y, "AB", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # x = 2./3*latt[0,0]+2./3*latt[0,1]
    # y = 2./3*latt[1,0]+2./3*latt[1,1]
    # ax_3.text(x,y, "BA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_4.text(x,y, "BA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')

    x = []
    y = []
    dx = []
    dz = []
    il = 3
    for n1 in range(-2,3):
        for n2 in range(-1,3):
            for ia in range(il*na_l,(il+1)*na_l):
                R = n1*latt[:,0]+n2*latt[:,1]
                xpos = R[0]+atoms_in[ia,0]
                ypos = R[1]+atoms_in[ia,1]
                if xmin-10<xpos and xpos<xmax+10 and ymin-10<ypos and ypos<ymax+10:
                    x.append(xpos)
                    y.append(ypos)
                    d1 = atoms_out[ia,0]-atoms_in[ia,0]
                    d2 = atoms_out[ia,1]-atoms_in[ia,1]
                    d3 = atoms_out[ia,2]-atoms_in[ia,2]
                    dx.append(sqrt(d1*d1+d2*d2))
                    dz.append(d3)
    x = array(x)
    y = array(y)
    dx = array(dx)
    dz = array(dz)

    if not quick:
        qv = ax_1.tripcolor(x,y,dx, rasterized=True, vmin=dxmin, vmax=dxmax, cmap=cmap)

    theta = 1.248242
    ax_1.text(0.20, 0.95, '$\\bf{\\theta=%.2f^\circ}$' % theta, transform=ax_1.transAxes,
      fontsize=8, fontweight='normal', va='top', color="k", ha='center')

    tx = 0.5
    ty = 1.09
    size = 8
    ax_1.text(tx, ty, 'In-plane', transform=ax_1.transAxes,
      fontsize=size, fontweight='bold', va='top', color="k", ha='center')
    ax_2.text(tx, ty, 'Out-of-plane', transform=ax_2.transAxes,
      fontsize=size, fontweight='bold', va='top', color="k", ha='center')
    ax_1.set_ylabel('Layer 1', fontweight='bold')
    ax_1.set_xlim(xmin,xmax)
    ax_1.set_ylim(ymin,ymax)
    ax_1.get_xaxis().set_ticks([])
    ax_1.get_yaxis().set_ticks([])

    # Moire unit cell
    points = [ [0,0],
               [latt[0,0],latt[1,0]],
               [latt[0,0]+latt[0,1],latt[1,0]+latt[1,1]],
               [latt[0,1],latt[1,1]]]
    sc = Polygon(points, True, facecolor='none', edgecolor='black', lw=.5)
    ax_1.add_patch(sc)
    sc = Polygon(points, True, facecolor='none', edgecolor='black', lw=.5)
    ax_2.add_patch(sc)
    sc = Polygon(points, True, facecolor='none', edgecolor='black', lw=.5)
    ax_3.add_patch(sc)
    sc = Polygon(points, True, facecolor='none', edgecolor='black', lw=.5)
    ax_4.add_patch(sc)
    tx = 0.5*(points[2][0]+points[3][0])
    ty = 0.5*(points[2][1]+points[3][1])+30
    ax_1.text(tx,ty, '$\\bf{L_M \\approx %d\;\AA}$'%linalg.norm(latt[:,0]),
            fontsize=8, color='k', horizontalalignment='center', verticalalignment='center')
    ax_1.scatter([0], [0], s=10, color='red')
    ax_1.text(-12, -25, 'AB/AB', color='red', va='center', ha='center', fontsize=8, fontweight='bold')
    tx = 1./3.*latt[0,0]+1./3.*latt[0,1]
    ty = 1./3.*latt[1,0]+1./3.*latt[1,1]
    ax_1.scatter([tx], [ty], s=10, color='blue')
    ax_1.text(tx+3, ty-20, 'AB/BC', color='blue', va='center', ha='center', fontsize=8, fontweight='bold')
    tx = 2./3.*latt[0,0]+2./3.*latt[0,1]
    ty = 2./3.*latt[1,0]+2./3.*latt[1,1]
    ax_1.scatter([tx], [ty], s=10, color='green')
    ax_1.text(tx, ty+18, 'AB/CA', color='green', va='center', ha='center', fontsize=8, fontweight='bold')

    if not quick:
        qv = ax_2.tripcolor(x,y,dz, rasterized=True, vmin=dxmin, vmax=dxmax, cmap=cmap)

    ax_2.set_xlim(xmin,xmax)
    ax_2.set_ylim(ymin,ymax)
    ax_2.get_xaxis().set_ticks([])
    ax_2.get_yaxis().set_ticks([])

    # fc = "white"
    # ax_1.text(0,0, "AA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_2.text(0,0, "AA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # x = 1./3*latt[0,0]+1./3*latt[0,1]
    # y = 1./3*latt[1,0]+1./3*latt[1,1]
    # ax_1.text(x,y, "AB", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_2.text(x,y, "AB", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # x = 2./3*latt[0,0]+2./3*latt[0,1]
    # y = 2./3*latt[1,0]+2./3*latt[1,1]
    # ax_1.text(x,y, "BA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')
    # ax_2.text(x,y, "BA", fontsize=12, color=fc, horizontalalignment='center', verticalalignment='center')

    plt.savefig('figure1.eps')
    plt.savefig('figure1.pdf')
    plt.show()

def read_atoms_out(fn):
    f = open(fn, 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    na = int(f.readline())

    data = []
    for l in f:
        row = float64(l.split())
        data.append(row)
    f.close()
    data = array(data)
    return latt, data

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower offset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax / (vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highest point in the colormap's range.
          Defaults to 1.0 (no upper offset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = hstack([
        linspace(0.0, midpoint, 128, endpoint=False),
        linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap
main()
